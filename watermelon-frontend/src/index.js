import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Navbar from './components/layout/Navbar'
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
        <Navbar/>
        <App/>
    </BrowserRouter>
    , document.getElementById('App'))
    , document.getElementById('Navbar')
    

