import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './components/Login';
import Admin from './components/Admin';
import Register from './components/Register';


class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route path="/admin" component={Admin} />
      </Switch>
    );
  }
}

export default App;