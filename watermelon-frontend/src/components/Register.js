import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'

export default class Register extends Component {
    constructor(props) {
        super(props)
        let SignIn = false
        this.state = {
            user_name: '',
            password: '',
            SignIn
        }

        this.onChange = this.onChange.bind(this)
        this.submitForm = this.submitForm.bind(this)
    }
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    submitForm(e) {
        const { user_name, password } = this.state
        // Register magic
        let url = "http://localhost:3000/user/add"
        let data = {
            "user_name": user_name,
            "password": password
        }
        axios
            .post("http://localhost:3000/user/add",
            {
                "user_name": user_name,
                "password": password
            }
        )
        .then(response => {
            console.log("res from register: ", response);
            if (response.data.response.status) {
                localStorage.setItem('session_token', response.data.response.sessiontoken)
                this.setState({
                    SignIn: true
                })
            }
        })
        .catch( error => {
            console.log("Register error", error.response);
        })
        e.preventDefault();
    }
    render() {
        if (this.state.SignIn) {
            alert("User added successful")
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                <form onSubmit={this.submitForm} className="white">
                    <h5 className="grey-text text-darken-3"> Sign up</h5>
                    <div className="form-field">
                        <label >User</label>
                        <input type="text" placeholder="User" name="user_name" className="form-control" value={this.state.user_name} onChange={this.onChange} />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Password" name="password" className="form-control" value={this.state.password} onChange={this.onChange} />
                    </div>
                    <button type="submit" className="btn pink lighten-1 z-depth-0">Sign up</button>

                </form>
            </div>
        )
    }
}