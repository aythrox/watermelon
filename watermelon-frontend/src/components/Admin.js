import React, { Component } from 'react'
import { Link, Redirect} from 'react-router-dom'
import axios from 'axios'

export default class Admin extends Component {
    constructor(props){
        super(props)
        const token = localStorage.getItem("session_token")
        let validToken = true
        let loggedIn = true
        this.state = {
            data: [],
            loggedIn,
            validToken
        }
        if (token == null) {
            loggedIn = false
        }
        axios
            .get("http://localhost:3000/rickandmorty/get",
            {
                "headers": {
                    "sessiontoken": token
                }
            }
        )
        .then(response => {
            if (response.data.response.status) {
                let datafiltered = response.data.response.response
                this.setState({
                    data: datafiltered,
                    loggedIn: true
                })
            }
        })
        .catch( error => {
            console.log("Error on GET characters", error.response);
            if (error.response.status === 401) {
                console.log("Token is Unauthorized")
                this.setState({
                    loggedIn: false,
                    validToken: false
                })
            }
        })
    }
    render() {
        if (this.state.validToken === false) {
            alert("Authorization token is invalid or expired, please Sign in");
        }
        if (this.state.loggedIn === false) {
            return <Redirect to='/' />
        }
        return (
            <div className="list-of-characters">
                {this.state.data.map(x => (
                    <div className="card z-depth-0">
                        <div className="card-content grey-text text-darken-3">
                        <img src={x.image}/>
                        <h1> {x.name} </h1>
                        <p className="black-text">
                            Status: {x.status}
                            <br/>
                            Species: {x.species}
                            <br/>
                            Gender: {x.gender} 
                        </p>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}