import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'

export default class Login extends Component {
    constructor(props) {
        super(props)
        let loggedIn = false
        this.state = {
            user_name: '',
            password: '',
            loggedIn,
            error: ''
        }

        this.onChange = this.onChange.bind(this)
        this.submitForm = this.submitForm.bind(this)
    }
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    submitForm(e) {
        const { user_name, password } = this.state
        // Login magic
        let url = "http://localhost:3000/user/login"
        let data = {
            "user_name": user_name,
            "password": password
        }
        axios
            .post("http://localhost:3000/user/login",
            {
                "user_name": user_name,
                "password": password
            }
        )
        .then(response => {
            console.log("res from login: ", response);
            if (response.data.response.status) {
                localStorage.setItem('session_token', response.data.response.sessiontoken)
                this.setState({
                    loggedIn: true
                })
            }
        })
        .catch( error => {
            console.log("Login error", error.response)
            this.setState({
                error: 'Error: ' + error.response.data.response.error
            })
        })
        e.preventDefault();
    }
    render() {
        if (this.state.loggedIn) {
            return <Redirect to="/admin" />
        }
        return (
            <div className="container">
                <form onSubmit={this.submitForm} className="white">
                    <h5 className="grey-text text-darken-3"> Sign in</h5>
                    <div className="form-field">
                        <label >User</label>
                        <input type="text" placeholder="User" name="user_name" className="form-control" value={this.state.user_name} onChange={this.onChange} />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Password" name="password" className="form-control" value={this.state.password} onChange={this.onChange} />
                    </div>
                    <button type="submit" className="btn pink lighten-1 z-depth-0">Sign In</button>
                    <p>{this.state.error}</p>
                </form>
                <div>
                    
                </div>

            </div>
        )
    }
}
/*
async function getDataAxios(data){
    const response = await axios.post(
        'http://localhost:3000/user/login',
        JSON.stringify(data),
        { headers: { 'Content-Type': 'application/json' } }
      )
      console.log(response.data)
      return response.data
}
*/
/*
function getDataFromFetech(url, data){
    let dataFromRequest = fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(response => response)

    dataFromRequest.then(
        console.log(dataFromRequest)
    )
    console.log(dataFromRequest)
    return dataFromRequest

}
*/