import React from 'react'
import { NavLink } from 'react-router-dom'

const SignedInLinks = () => {
    return (
        <ul>
            <li className="left"><NavLink to='/' className="brand-logo">Watermelon API</NavLink></li>
            <li className="right"><NavLink to='/admin'>Hit the API!</NavLink></li>
            <li className="right"><NavLink to='/register'>Register</NavLink></li>
            <li className="right"><NavLink to='/'>Login</NavLink></li>
        </ul>
    )
}
export default SignedInLinks