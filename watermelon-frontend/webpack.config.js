var path = require('path');
var HtmlWebpackPlugin =  require('html-webpack-plugin');

module.exports = {
    entry :
        './src/index.js',
    output : {
        path : path.resolve(__dirname , 'dist'),
        filename: 'app.js'
    },
    module : {
        rules : [
            {test : /\.(js)$/, use:'babel-loader'},
            {test : /\.css$/, use:['style-loader', 'css-loader']}
        ]
    },
    
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 8080,
        historyApiFallback: true,
    },
    
    plugins : [
        new HtmlWebpackPlugin ({
            template : './public/index.html'
        })
    ]
}