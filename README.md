REQUESITES:
- Docker
- Docker-compose

RUN: docker-compose up

USEFUL COMMANDS:
Access to redis-clid: docker exec -it [container_id] redis-cli 

Frontend is running on port 8080
Backend is running on port 3000
Redis expose 6379 to backend

Frontend is using webpack.

Multistage is not configurated.