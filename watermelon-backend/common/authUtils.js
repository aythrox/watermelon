import uuid from 'node-uuid';
const newSessionRoutes = [{ path: '/user/login', method: 'POST' }, { path: '/user/login', method: 'OPTIONS'}];
const authRoutes = [{ path: '/user/password', method: 'PUT' }, { path: '/rickandmorty/get', method: 'GET'}];

export const isNewSessionRequired = (httpMethod, url) => { 
    for (let routeObj of newSessionRoutes) {
        if (routeObj.method === httpMethod && routeObj.path === url) {
            return true;
        }
    }
    return false;
}
export const isAuthRequired = (httpMethod, url) => {
    for (let routeObj of authRoutes) {
        if (routeObj.method === httpMethod && routeObj.path === url) {
            return true;
        }
    }
    return false;
}
export const generateRandomSessionID = () => {
    return uuid.v4();
}
