import { isNewSessionRequired, isAuthRequired, generateRandomSessionID } from '../common/authUtils';
import Session from '../common/Session'
import Redis from 'ioredis';
const redis = new Redis(6379, 'redis');

module.exports = {
    sessionValidation: async function (req, res, next) {
        let apiUrl = req.originalUrl;
        let httpMethod = req.method;
        if (isNewSessionRequired(httpMethod, apiUrl)) {
            let sessionID = generateRandomSessionID();
            req.session = new Session();
            req.session.sessionID = sessionID;
            req.sessionID = sessionID;
        } else if (isAuthRequired(httpMethod, apiUrl)) {
            let sessionID = req.header('sessiontoken');
            if (sessionID) {
                let redisData = await redis.get(sessionID);
                if (redisData) {
                    redisData = JSON.parse(redisData);
                    req.session = new Session();
                    req.sessionID = sessionID;
                    req.session.sessionID = sessionID;
                    req.session.userData = redisData;
                } else {
                    return res.status(401).send({
                        ok: false,
                        error: {
                            reason: "Invalid Sessiontoken",
                            code: 401
                        }
                    });
                }
            } else {
                return res.status(401).send({
                    ok: false,
                    error: {
                        reason: "Missing Sessiontoken",
                        code: 401
                    }
                });
            }
        }
        next();
    },
    endPointsAndValidToken: async function (req, res, next) {
        if (!res.data) {
            console.log("No res.data");
            return res.status(404).send({
                ok: false,
                error: {
                    reason: "Invalid Endpoint", code: 404
                }
            });
        }
        if (req.session && req.sessionID) {
            try {
                req.session.save(redis);
                res.setHeader('sessiontoken', req.sessionID);
                res.data['sessiontoken'] = req.sessionID;
            } catch (e) {
                console.log('Error ->:', e);
            }
        }
        res.status(res.statusCode || 200)
            .send({ ok: true, response: res.data });
    }
}