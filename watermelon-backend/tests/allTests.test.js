const app = require("../server"); // Link to server file
const supertest = require("supertest");
const request = supertest(app);
let tokenAuth
it("Should save user in Redis", async (done) => {
    const res = await request
        .post("/user/add")
        .send({
            user_name: "testname",
            password: "testpassword"
        });
    expect(res.statusCode).toEqual(200)
    done();
});

it("Should login with username and password previus created", async (done) => {
    const res = await request
        .post("/user/login")
        .send({
            user_name: "testname",
            password: "testpassword"
        });
    tokenAuth = res.body.response.sessiontoken
    expect(res.statusCode).toEqual(200)
    done();
});

it("Trying to get Rick and Morty characters", async (done) => {
    const res = await request
        .get("/rickandmorty/get")
        .set('sessiontoken', tokenAuth)
    expect(res.statusCode).toEqual(200)
    done();
});


afterAll(() => setTimeout(() => process.exit('Tests done...'), 1000))