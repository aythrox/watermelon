import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

let userRouter = require('./routes/user');
let rickandmortyRouter = require('./routes/rickandmorty')

let middlewares = require('./middlewares/middleware')

const app = express();

app.use(cors());
app.options('*', cors())

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.options("/*", function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.send(200);
  });

app.use(middlewares.sessionValidation)

app.get('/', function (req, res, next) {
    res.status(200).send({
      status: true,
      response: 'Working'
    })
});
app.use('/user', userRouter);
app.use('/rickandmorty', rickandmortyRouter);

app.use(middlewares.endPointsAndValidToken)

app.listen(3000, () => {
  console.log(' Watermelon API is running on port 3000');
})
process.on('exit', (code) => {
  console.log(`About to exit with code: ${code}`);
});
process.on('SIGINT', function () {
  console.log("Caught interrupt signal");
  process.exit();
});

module.exports = app;