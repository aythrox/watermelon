import axios from 'axios';

exports.getCharacters =  async function (req, res, next) {
    await axios.get('https://rickandmortyapi.com/api/character')
        .then(response => {
            let data = response.data.results;
            let dataFiltered = data.map(x => {
                return {
                    'name': x.name,
                    'status': x.status,
                    'species': x.species,
                    'gender': x.gender,
                    'image': x.image
                }
            })
            res.statusCode = 200;
            res.data = {
                status: true,
                message: 'Characters Successful obtained',
                response: dataFiltered
                
            };
        })
        .catch(error => {
            console.log(error);
            res.statusCode = 400;
            res.data = {
                status: false,
                message: 'Error on request'
            };
        })
        next();
};