import Redis from 'ioredis';
const redis = new Redis(6379, 'redis');


exports.addUser = async function (req, res, next) {
    console.log('req.body: ', req.body);
    const uname = req.body.user_name;
    const pwd = req.body.password;
    const data = {
        'password': pwd
    };
    try {
        const result = await redis.set(uname, JSON.stringify(data));

        // Turn around and bring back Shamu immediately to prove it works.
        const parsedData = JSON.parse(await redis.get(uname));
        console.log('parsed data :', parsedData);
        res.data = {
            status: true,
            response: parsedData,
            message: "User added successfully"
        }
    } catch (e) {
        console.log(e);
        res.statusCode = 400;
        res.data = {
            status: false,
            error: 'Error when adding new user'
        };
    }
    next();
}

exports.login = async function (req, res, next) {
    let uname = req.body.user_name;
    let pwd = req.body.password;
    let jsonData = JSON.parse(await redis.get(uname));
    if (jsonData) {
        jsonData.user_name = uname;
        if (pwd === jsonData.password) {
            res.statusCode = 200;
            res.data = {
                status: true,
                user: jsonData
            };
            req.session.set(jsonData);
        } else {
            res.statusCode = 400;
            res.data = {
                status: false,
                error: 'Invalid Password'
            };
        }
    } else {
        res.statusCode = 400;
        res.data = {
            status: false,
            error: 'Invalid Username'
        };
    }
    next();
};

exports.updatePassword = async function (req, res, next) {
    try {
        let oldPwd = req.body.old_password;
        let newPwd = req.body.new_password;
        if (!oldPwd && !newPwd) {
            res.statusCode = 400;
            res.data = {
                status: false,
                error: 'Invalid Parameters'
            }
        }

        let uname = req.session.userData.user_name;
        let userDetails = JSON.parse(await redis.get(uname));
        if (userDetails !== null) {
            if (oldPwd !== userDetails.password) {
                res.statusCode = 400;
                res.data = {
                    status: false,
                    error: "Old Password doesn't match"
                }
            } else {
                let newPassword = {
                    "password": newPwd
                }
                let updatedRes = await redis.set(uname, JSON.stringify(newPassword));
                res.statusCode = 200;
                res.data = {
                    status: true,
                    response: updatedRes,
                    message: "Password updated successfully"
                }
            }
        } else {
            res.statusCode = 401;
            res.data = {
                status: false,
                error: 'User token is invalid'
            }
        }

        next();
    } catch (e) {
        next(e)
    }
}