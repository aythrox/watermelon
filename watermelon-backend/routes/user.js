import { Router } from 'express';
let router = Router();
let user_controller = require('../controllers/userController');

router.post('/add', user_controller.addUser);
router.post('/login', user_controller.login)
router.post('/password', user_controller.updatePassword)

module.exports = router;