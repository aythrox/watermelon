import { Router } from 'express';
let rickandmorty_controller = require('../controllers/rickandmortyController');
let router = Router();

router.get('/get', rickandmorty_controller.getCharacters)

module.exports = router;